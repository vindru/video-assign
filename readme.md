Seed Database before using

php artisan db:seed

1. Get request endpoint for video size created by user

/api/videosize/{id}

2. Get request endpoint for video meta data

/api/videodetail/{id}

3. Patch request for update

/api/videometa

parameter: video_id ,video_size,viewers_count