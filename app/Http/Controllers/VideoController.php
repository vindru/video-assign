<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Http\Request;
use App\User;
use App\Video;

class VideoController extends Controller
{
    public function size($id){
        try { 
            $size = User::findorfail($id)->videos->pluck('video_size')->sum();
        }
        catch (ModelNotFoundException $exception) {
            return "Failed";
        }
            return $size;
    }

    public function detail($id){
        try { 
        $size = Video::findorfail($id)
        ->leftJoin('users', 'videos.user_id', '=', 'users.id')
        ->select(
            'users.user_name',
            'videos.video_size',
            'videos.viewers_count')
        ->first();
        }
        catch (ModelNotFoundException $exception) {
            return "Failed";
        }

        return $size;
    }

    public function meta(Request $request){
        try { 
        Video::findorfail($request->video_id)
             ->update([
                'video_size' => $request->video_size,
                'viewers_count'=>$request->viewers_count 
                ]);
             }
                catch (ModelNotFoundException $exception) {
                    return "Failed";
                     }
        return "Success";
    }
}
