<?php

use Illuminate\Database\Seeder;
use App\Video;
use App\User;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->delete();
        $userIds = User::pluck('id');
        $videos = [
            ['video_name' => 'Video1','video_size'=>120,'viewers_count'=>1100,'user_id' => $userIds[0]],
            ['video_name' => 'Video2','video_size'=>80,'viewers_count'=>2000,'user_id' => $userIds[0]],
            ['video_name' => 'Video3','video_size'=>250,'viewers_count'=>900,'user_id' => $userIds[0]],
            ['video_name' => 'Video4','video_size'=>90,'viewers_count'=>600,'user_id' => $userIds[1]],
            ['video_name' => 'Video5','video_size'=>75,'viewers_count'=>700,'user_id' => $userIds[1]],
            ['video_name' => 'Video6','video_size'=>300,'viewers_count'=>3000,'user_id' => $userIds[1]],
            ['video_name' => 'Video7','video_size'=>200,'viewers_count'=>2200,'user_id' => $userIds[2]],
        ];
        Video::insert($videos);
    }
}
