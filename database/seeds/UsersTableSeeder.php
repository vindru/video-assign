<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $users = [
            ['user_name' => 'User1'],
            ['user_name' => 'User2'],
            ['user_name' => 'User3'],
        ];
        
        User::insert($users);
    }
}
